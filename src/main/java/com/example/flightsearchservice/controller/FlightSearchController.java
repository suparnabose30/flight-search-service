package com.example.flightsearchservice.controller;

import com.example.flightsearchservice.model.Flight;
import com.example.flightsearchservice.repository.FlightSearchRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RefreshScope
@RestController
@RequestMapping("/api/v1")
@Tag(name = "flightSearch", description = "The FlightSearch API")
public class FlightSearchController {
    protected static Logger logger = LoggerFactory.getLogger(FlightSearchController.class.getName());
    @Autowired
    private FlightSearchRepository flightSearchRepo;

    @Value("${msg}")
    String msg;

    @GetMapping("/hello")
    public ResponseEntity<String> getHello() {
        return new ResponseEntity<String>( msg, HttpStatus.OK);
    }

    @Operation(summary = "Add a new Flight Details", description = "", tags = { "flightSearch" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Flight added",
                    content = @Content(schema = @Schema(implementation = Flight.class))),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "Flight already exists") })

    @RequestMapping(value="/createFlightEntry", method = RequestMethod.POST)
    public ResponseEntity<Flight> createFlightEntry(@Parameter(description="Flight details to add. Cannot null or empty.",
            required=true, schema=@Schema(implementation = Flight.class))
                                                          @Valid @RequestBody Flight flightSearch){
        try{
            return new ResponseEntity<>(flightSearchRepo.save(flightSearch), HttpStatus.CREATED);
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Operation(summary = "Get Flight details by id" , description = "", tags = { "flightSearch" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Flight details received by Id",
                    content = @Content(mediaType = "application/xml",schema = @Schema(implementation = Flight.class))),
            @ApiResponse(responseCode = "404", description = "NOT FOUND", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content) })
    @RequestMapping(value="/flightSearch/{id}", method = RequestMethod.GET)
    public ResponseEntity<Flight> getFlightByFlightId(@PathVariable Long id) {
        Optional<Flight> flightSearch = flightSearchRepo.findById(id);

        if (flightSearch.isPresent()) {
            return new ResponseEntity<>(flightSearch.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Get all Flights" , description = "", tags = { "flightSearch" })
    @RequestMapping(value="/getAllFlights", method = RequestMethod.GET)
    public ResponseEntity<List<Flight>> getAllFlights() {
        try {
            List<Flight> list = flightSearchRepo.findAll();
            if (list.isEmpty() || list.size() == 0) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Get Flight availability by id" , description = "", tags = { "flightSearch" })
    @RequestMapping(value="/getAvailabilityById/{id}", method = RequestMethod.GET)
    public int getAvailabilityById(@PathVariable long id) {
        try {
            Optional<Flight> searchedFlightDetails = flightSearchRepo.findById(id);
            if (!searchedFlightDetails.isPresent()) {
                return 0;
            }
            return searchedFlightDetails.get().getAvailability();
        } catch (Exception e) {
            return 0;
        }
    }

    @Operation(summary = "Increase Flight availability by id and availability count" , description = "", tags = { "flightSearch" })
    @PutMapping("/increaseFlightAvailability/{id}/{availability}")
    public ResponseEntity<Flight> increaseFlightAvailability(@PathVariable long id, @PathVariable int availability) {
        try {
            int currentAvaliability = getAvailabilityById(id);
            ResponseEntity<Flight> searchedFlightDetails =getFlightByFlightId(id);
            searchedFlightDetails.getBody().setAvailability(availability + currentAvaliability);
            return new ResponseEntity<>(flightSearchRepo.save(searchedFlightDetails.getBody()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Decrease Flight availability by id and availability count" , description = "", tags = { "flightSearch" })
    @PutMapping("/decreaseFlightAvailability/{id}/{availability}")
    public ResponseEntity<Flight> decreaseFlightAvailability(@PathVariable long id, @PathVariable int availability) {
        try {
            int currentAvaliability = getAvailabilityById(id);
            ResponseEntity<Flight> searchedFlightDetails =getFlightByFlightId(id);
            searchedFlightDetails.getBody().setAvailability(currentAvaliability - availability);
            return new ResponseEntity<>(flightSearchRepo.save(searchedFlightDetails.getBody()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

