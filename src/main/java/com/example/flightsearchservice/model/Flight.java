package com.example.flightsearchservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "flightSearch")
@Setter
@Getter
@ToString
public class Flight implements Serializable {

    private static final long serialVersionUID = 4048798961366546485L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @JsonProperty("flightId")
    @NotEmpty(message = "Flight ID Is Required")
    @Size(max = 10)
    @Column(name="flightId")
    private Long flightId;

    @JsonProperty("flightName")
    @Column(name="flightName")
    private String flightName;

    @Column(name="availability")
    private int availability;

    @Column(name="start_date")
    private Date start_date;
}
