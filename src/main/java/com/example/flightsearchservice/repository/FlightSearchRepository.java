package com.example.flightsearchservice.repository;

import com.example.flightsearchservice.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightSearchRepository extends JpaRepository<Flight,Long> {
}
